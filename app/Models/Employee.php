<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table = 'employees';
    protected $guarded = [];

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function skills()
    {
        return $this->hasMany(EmployeeSkill::class, 'employee_id', 'id');
    }
}
