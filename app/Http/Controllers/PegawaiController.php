<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\EmployeeSkill;
use App\Models\Position;
use App\Models\Skill;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Employee::with('position')->get();
        return view('pegawai.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $position   = Position::orderBy('name')->get();
        $skill      = Skill::orderBy('name')->get();
        return view('pegawai.create', compact('position', 'skill'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = Employee::create([
            'position_id'   => $request->position,
            'name'          => $request->name,
            'birth'         => $request->birth,
            'gender'        => $request->gender,
            'phone'         => $request->phone,
            'address'       => $request->address,
            'status'        => 1,
        ]);

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $ext = $file->getClientOriginalExtension();
            $newName = Str::slug($request->name).'-'.md5(uniqid(rand(), true)).'.'.$ext;
            $path = public_path("images/");
            $file->move($path, $newName);
            $data->photo = $newName;
            $data->save();
        }

        if (count($request->skill) != 0) {
            foreach ($request->skill as $value) {
                EmployeeSkill::create([
                    'employee_id'   => $data->id,
                    'skill_id'      => $value
                ]);
            }
        }

        return redirect('employee');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = Employee::with('position', 'skills')
            ->whereId($id)
            ->first();

        return view('pegawai.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
