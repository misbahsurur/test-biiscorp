<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employee>
 */
class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'position_id' => rand(1,9),
            'name' => fake()->name(),
            'birth' => fake()->date(),
            'gender' => fake()->randomElement(['Male' ,'Female']),
            'phone' => fake()->phoneNumber(),
            'address' => fake()->address(),
            'status' => 1,
        ];
    }
}
