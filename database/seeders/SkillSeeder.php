<?php

namespace Database\Seeders;

use App\Models\Skill;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['name'=>'Clean Code'],
            ['name'=>'Communication'],
            ['name'=>'Analytical abilities'],
            ['name'=>'Cloud computing'],
            ['name'=>'Networking'],
            ['name'=>'Project management'],
            ['name'=>'Machine learning'],
            ['name'=>'Creativity'],
            ['name'=>'Operating systems'],
            ['name'=>'UI/UX design'],
            ['name'=>'Cybersecurity'],
            ['name'=>'Leadership'],
            ['name'=>'Problem solving'],
            ['name'=>'Database management'],
            ['name'=>'DevOps'],
            ['name'=>'Organization'],
            ['name'=>'Software development'],
            ['name'=>'Customer relationship management'],
            ['name'=>'Visualization'],
        ];

        Skill::insert($data);
    }
}
