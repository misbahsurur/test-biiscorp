<?php

namespace Database\Seeders;

use App\Models\EmployeeSkill;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmployeeSkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        EmployeeSkill::factory()
            ->count(250)
            ->create();
    }
}
