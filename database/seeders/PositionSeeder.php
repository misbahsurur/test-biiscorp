<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['name'=>'IT technician'],
            ['name'=>'Support specialist'],
            ['name'=>'Quality assurance tester'],
            ['name'=>'Web developer'],
            ['name'=>'Mobile developer'],
            ['name'=>'IT security specialist'],
            ['name'=>'Computer programmer'],
            ['name'=>'Systems analyst'],
            ['name'=>'Network engineer'],
        ];

        Position::insert($data);
    }
}
