@extends('layouts.main')
@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Create Employee</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Employee</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- // Basic multiple Column Form section start -->
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form id="formEmployee" class="form" action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('POST')
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group mandatory">
                                            <label class="form-label" for="name">Name</label>
                                            <input type="text" id="name" class="form-control" placeholder="Name" name="name">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group mandatory">
                                            <label class="form-label" for="birth">Date of Birth</label>
                                            <input type="text" id="birth" class="form-control" name="birth">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group mandatory">
                                            <label class="form-label w-100" for="gender">Gender</label>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="Female">
                                                <label class="form-check-label" for="inlineRadio1">Female</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="Male">
                                                <label class="form-check-label" for="inlineRadio2">Male</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group mandatory">
                                            <label class="form-label" for="position">Position</label>
                                            <select id="position" class="form-control" name="position">
                                                <option value=""></option>
                                                @foreach ($position as $option)
                                                    <option value="{{ $option->id }}">{{ $option->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group mandatory">
                                            <label class="form-label" for="phone">Phone</label>
                                            <input type="text" id="phone" class="form-control" name="phone" placeholder="Phone Number">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label class="form-label" for="skill">Skills</label>
                                            <select class="form-control" multiple="multiple" name="skill[]" id="skill">
                                                @foreach ($skill as $option)
                                                    <option value="{{ $option->id }}">{{ $option->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label class="form-label" for="address">Address</label>
                                            <textarea name="address" id="address" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label class="form-label" for="photo">Photo</label>
                                            <input type="file" name="photo" id="photo">
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-end">
                                        <button type="submit" class="btn btn-primary me-1 mb-1">Submit</button>
                                        <button type="reset" class="btn btn-danger me-1 mb-1">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- // Basic multiple Column Form section end -->
</div>
@endsection
@section('styles')
<link rel="stylesheet" type="text/css" media="all" href="{{ asset('assets/daterangepicker.css') }}" />
<link rel="stylesheet" type="text/css" media="all" href="{{ asset('assets/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" media="all" href="{{ asset('assets/fileinput/fileinput.min.css') }}" />
<style>
    .select2-container {
        width: 100% !important;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/fileinput/fileinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/validation/additional-methods.js') }}"></script>
<script>
    $(function() {
        $('input[name="birth"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale:{
                format: "YYYY-MM-DD",
            }
        });

        $("#position").select2({
            placeholder: "Select an Option",
            allowClear: true
        });

        $("#skill").select2({
            placeholder: "Select an Option",
        });

        $("#photo").fileinput({
            showUpload: false,
            dropZoneEnabled: false,
            maxFileCount: 1,
            allowedFileExtensions: ['jpg', 'jpeg', 'png'],
            maxFileSize: 400,
        });

        $("#formEmployee").validate({
            errorElement: 'em',
            errorClass: "is-invalid",
            validClass: "is-valid",
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                birth: {
                    required: true,
                    date: true
                },
                gender: "required",
                position: "required",
                phone: {
                    required: true,
                    number: true,
                    rangelength: [9, 14]
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection
