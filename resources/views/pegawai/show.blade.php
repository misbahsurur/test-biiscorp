<div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel4">Detail {{ $data->name }}</h4>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
            </button>
        </div>
        <div class="modal-body">
            <div class="text-center">
                <img src="{{ $data->photo != null ? asset('images/'.$data->photo) : asset('assets/img/no-image.jpg') }}" alt="{{ $data->name }}" class="mb-3" width="200px">
            </div>
            <table class="table table-borderless w-100">
                <tr class="mb-3">
                    <th class="text-muted">Name</th>
                    <th>: {{ $data->name }}</th>
                </tr>
                <tr class="mb-3">
                    <th class="text-muted">Position</th>
                    <th>: {{ $data->position->name }}</th>
                </tr>
                <tr class="mb-3">
                    <th class="text-muted">Date of Birth</th>
                    <th>: {{ date('d F Y', strtotime($data->birth)) }}</th>
                </tr>
                <tr class="mb-3">
                    <th class="text-muted">Gender</th>
                    <th>: {{ $data->gender }}</th>
                </tr>
                <tr class="mb-3">
                    <th class="text-muted">Phone</th>
                    <th>: {{ $data->phone }}</th>
                </tr>
                <tr class="mb-3">
                    <th class="text-muted">Address</th>
                    <th>: {{ $data->address }}</th>
                </tr>
                <tr class="mb-3">
                    <th class="text-muted">Skill</th>
                    <th>
                        :@foreach ($data->skills as $li)
                        <span class="badge bg-primary me-1 mb-1">{{ $li->skill->name }}</span>
                        @endforeach
                    </th>
                </tr>
                <tr>
                    <th class="text-muted">Status</th>
                    <th>:
                        @if ($data->status == 1)
                        <span class="badge bg-success">Active</span>
                        @else
                        <span class="badge bg-danger">Inactive</span>
                        @endif
                    </th>
                </tr>
            </table>
        </div>
    </div>
</div>
